FROM openjdk:15.0.1-slim

LABEL maintainer="Kushal Karmakar"
LABEL email="kushal@thirdeyedata.io"

# Never prompt the user for choices on installation/configuration of packages
ENV DEBIAN_FRONTEND noninteractive
ENV TERM linux

# Define en_US.
ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV LC_CTYPE en_US.UTF-8
ENV LC_MESSAGES en_US.UTF-8


# Setting up environment
ARG ML_USER_HOME=/usr/local/app/

# Creating a directory for entire java code
# RUN mkdir /app
# WORKDIR /app

# Installing wget and unzip package
RUN apt-get update && apt-get -y install wget && apt-get -y install unzip && apt-get clean

# Downloading and unzipping the gradle 6.8.2 package
RUN wget https://services.gradle.org/distributions/gradle-6.8.2-bin.zip -P /tmp
RUN unzip -d /opt/gradle /tmp/gradle-*.zip

# Setting up the GRADLE HOME and adding to system path
ENV GRADLE_HOME=/opt/gradle/gradle-6.8.2
ENV PATH=${GRADLE_HOME}/bin:${PATH}

# Copying the entire java code to /app directory
COPY src ${ML_USER_HOME}/src
COPY build.gradle settings.gradle ${ML_USER_HOME}

# Building the Java project and creating a JAR file
WORKDIR ${ML_USER_HOME}
RUN gradle clean
RUN gradle build -x test
RUN gradle clean jar

# Exposing the Docker Port
EXPOSE 9700

# Adding entrypoing for the Java execution
ENTRYPOINT ["java" , "-jar" , "build/libs/ML-commons-0.0.1-SNAPSHOT.jar"]